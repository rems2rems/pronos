import { getMatch, getMatchs } from '../services/matchs.js'
const showMatchs = async (req, res) => {
    
    try {
        const matchs = await getMatchs()
        res.render('matchs', { matchs,user:req.session.user })
    }
    catch (err) {
        throw err
    }
}

const showMatch = async (req, res) => {

    const matchId = req.params.matchId
    try {
        const match = await getMatch(matchId)
        res.render('match', { match, user: req.session.user })
    }
    catch (err) {
        throw err
    }
}

export { showMatchs,showMatch }