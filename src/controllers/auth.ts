import { addUser, getUser, hasAdmin } from '../services/users.js'
import { LoginData, SignupData, User } from '../models/app/user.js'
import { compare,hash } from 'bcrypt'
import createError from 'http-errors'
import { StatusCodes } from 'http-status-codes'

const signup = async (req, res,next) => {

    console.log("signup!");

    try {
        const data: SignupData = req.body
        if (data.password !== data.repeatPassword) {
            next(createError(StatusCodes.BAD_REQUEST, "passwords do not match."))
            return
        }
        let user: User = {
            pseudo: data.pseudo, email: data.email,
            pwdHash: await hash(data.password, parseInt(process.env.NB_SALT_ROUNDS)), roles: ["user"]
        }
        if (!await hasAdmin()) {
            user.roles.push("admin")
        }
        await addUser(user)
        req.session.user = user
        res.redirect('/')
    }
    catch (err) {
        throw err
    }
}

const login = async (req, res,next) => {

    console.log("login!");
    
    const data : LoginData = req.body
    try {
        const user = await getUser(data.email)
        if (!user) {
            // next(createError(StatusCodes.BAD_REQUEST, "invalid user/password."))
            res.render("showLogin",{showError:true})
            return
        }
        console.log("hash",user.pwdHash);
        console.log("pass",data.password);
        
        const pwdOk = await compare(data.password, user.pwdHash)
        console.log("pwdok",pwdOk);
        
        if (!pwdOk) {
            // next(createError(StatusCodes.BAD_REQUEST, "invalid user/password."))
            res.render("showLogin", { showError: true })
            return
        }
        console.log("login OK");
        
        req.session.user = user
        res.redirect('/')
    }
    catch (err) {
        console.log(err.message);
        throw err
    }
}

export { signup,login }