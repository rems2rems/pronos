type Match = {
    team1: string,
    team2: string,
    winner: "team1" | "team2" | "none" | "unknown",
    status : string
}

export { Match }