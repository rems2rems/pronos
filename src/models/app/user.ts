type Role = "admin" | "user"

interface OtherUser {
    email: string,
    pseudo: string,
    roles: Array<Role>
}

interface User extends OtherUser {
    pwdHash:string
}

interface SignupData {
    email: string,
    pseudo: string,
    password: string,
    repeatPassword: string
}

interface LoginData {
    email: string,
    password: string,
}

export { User,OtherUser,SignupData,LoginData,Role }