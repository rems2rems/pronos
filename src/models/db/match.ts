import { model, Schema } from 'mongoose'
import { Id } from '../app/id.js'
import { Match } from '../app/match.js'
const ObjectId = Schema.Types.ObjectId

const MatchSchema = new Schema<Match>({
    team1: String,
    team2: String,
    winner: {
        type: String,
        enum : ["team1","team2","none","unknown"]
    }
})

const MatchModel = model<Match & Id>("match", MatchSchema)

export {MatchSchema, MatchModel as Match}