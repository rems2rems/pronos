import { model, Schema } from 'mongoose'
import { Id } from '../app/id.js'
import { User } from '../app/user.js'

const ObjectId = Schema.Types.ObjectId

const UserSchema = new Schema<User>({
    pseudo: String,
    email: String,
    roles: [{type:String,enum:["admin","user"]}],
    pwdHash : String
})

// const OtherUserModel = model<OtherUser>("match", OtherUserSchema)
const UserModel = model<User & Id>("user", UserSchema)

export { UserSchema,UserModel as User }