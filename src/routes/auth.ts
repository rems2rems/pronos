import { Router } from 'express'
import { signup,login } from '../controllers/auth.js'

const router = Router()

router.get('/signup',(req,res)=>{ res.render('showSignup')})
router.post('/signup',signup )
router.get('/login', (req, res) => { res.render('showLogin',{showError:false}) })
router.post('/login', login)
router.all('/logout', (req: any,res) => {
    req.session?.destroy()
    res.redirect('/login')
})
export default router