import { StatusCodes } from 'http-status-codes'
import createError from 'http-errors'

const isAdmin = (req, res, next) => {
    if (!req.session.user?.roles.include("admin")) {
        next(createError(StatusCodes.FORBIDDEN))
        return
    }
    next()
}

const isAuth = (req, res, next) => {
    if (!req.session.user) {
        next(createError(StatusCodes.UNAUTHORIZED))
        return
    }
    next()
}

export { isAdmin,isAuth}