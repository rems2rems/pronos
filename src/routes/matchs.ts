import { Router } from 'express'
import { showMatch, showMatchs } from '../controllers/matchs.js'
import { isAdmin, isAuth } from './guards/auth.js'

const router = Router()

router.get('/:matchId', isAuth, showMatch)
router.get('/', isAuth, showMatchs)

/////////
//TODO : add addMatch to controller, etc.
// router.post('/', isAuth,isAdmin, addMatch)
/////////

export default router