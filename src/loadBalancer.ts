import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import axios from 'axios';
import request from 'request';
const servers = process.env.CLUSTERED_IPS.split(",");
let activeServers = null

async function updateServers() {
    activeServers = [...servers]
    for (let server of servers) {
        console.log("testing...", server);
        try {
            const res = await axios.get(server + '/hello')
            if (!res || res.data != "world!") {
                activeServers.splice(servers.indexOf(server), 1)
            }
        } catch (error) {
            console.log("axios err", error.message);
            activeServers.splice(servers.indexOf(server), 1)
        }
    }
}
let cur = 0;
const handler = async (req  :express.Request, res) => {
    await updateServers()
    if (cur >= activeServers.length) {
        cur = 0
    }
    try {
        console.log("active:", activeServers);
        console.log("receved a request... passing...");
        // Pipe the vanilla node HTTP request (a readable stream) into `request`
        // to the next server URL. Then, since `res` implements the writable stream
        // interface, you can just `pipe()` into `res`.
        req.pipe(request({ url: activeServers[cur] + req.url })).pipe(res);
        cur = (cur + 1) % activeServers.length;
    }
    catch (err) {
        console.log("handler err", err.message);
    }
};
const server = express()

server.all('*', handler);

// setInterval(async () => {
//     try {
//         updateServers()
//     }
//     catch (err) {
//         console.log("interv err",err.message);
//     }
// }, 5000)

server.listen(8080, () => {
    console.log("load balancer started...");
});