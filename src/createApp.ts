import express from 'express'
import matchsRouter from './routes/matchs.js'
import authRouter from './routes/auth.js'
import session from 'express-session'
import cookieParser from 'cookie-parser'
import { StatusCodes } from 'http-status-codes';
import helmet from 'helmet'
import favicon from 'serve-favicon'
import {dirname,join} from 'node:path'

function createApp() {
    
    const app = express()
    app.set('view engine', 'ejs')
    app.set('views', 'src/views')
    
    app.use(helmet())    
    app.use(express.static('src/public'))
    app.use(favicon(join(dirname('.'), 'src/public', 'favicon.ico')))
    app.use(cookieParser())
    app.use(express.urlencoded({ extended: false }))
    app.use(session({ secret: process.env.SESSION_SECRET, cookie: { maxAge: 60000,sameSite:"strict" }, resave: true, saveUninitialized: true }))
    app.use((req: express.Request, res, next) => {
        
        console.log("request managed by server ", req.hostname,":", process.env.PORT);
        next()
    })


    app.get('/hello', (req, res) => {
        res.send('world!')
    })
    app.get('/', (req, res) => {
        res.redirect('/matchs')
    })
    app.use('/matchs', matchsRouter)
    app.use('/', authRouter)
    
    app.use((err, req, res: express.Response, next) => {
        if (res.headersSent) {
            next(err)
            return
        }

        if (err.statusCode === StatusCodes.FORBIDDEN || err.statusCode === StatusCodes.UNAUTHORIZED) {
            res.redirect('/login')
            return
        }
        next(err)
    })
    
    return app
}
export default createApp