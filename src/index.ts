import dotenv from 'dotenv'
dotenv.config()

import mongoose from 'mongoose'
import createApp from './createApp.js'
import { Match } from './models/app/match.js'
import { addMatch, getMatchs } from './services/matchs.js'

mongoose.set('strictQuery', false)

console.log("connecting database...");

mongoose.connect(process.env.DB, { serverSelectionTimeoutMS: 1000 })
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'error connecting with mongodb database:'));
db.once('open', function () {
    console.log('connected to database');
    const app = createApp()
    app.listen(process.env.PORT, async () => {
        console.log("server started...");
        let matches = await getMatchs()
        if (matches.length == 0) {
            let match: Match = {team1: "France",team2:"Argentine",winner:"unknown",status:"to-be-played"}
            let dbMatch  = await addMatch(match)
            console.log("added match",dbMatch._id);
            
        }
    })
});