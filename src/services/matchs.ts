import { Id } from '../models/app/id.js';
import { Match } from '../models/app/match.js'
import { Match as DbMatch } from '../models/db/match.js';

async function getMatchs(): Promise<(Match & Id)[]> {
    return DbMatch.find()
}

async function getMatch(matchId): Promise<(Match & Id)> {
    return DbMatch.findOne({_id:matchId})
}

async function addMatch(match): Promise<Match & Id> {
    const dbMatch = new DbMatch(match)
    return dbMatch.save()
}

export { getMatchs, addMatch,getMatch }