import { Match } from '../models/app/match.js'
import { User } from '../models/app/user.js';
import { User as DbUser } from '../models/db/user.js';

async function getUser(email: string): Promise<User> {
    const user = await DbUser.findOne({ email: email })
    console.log(user);
    return user
}

async function hasAdmin(): Promise<Boolean> {
    const nbAdmins = await DbUser.count({
        $expr: {
            $in: ["admin", "$roles"]
        }
    })
    console.log(nbAdmins);
    return nbAdmins > 0
}

async function addUser(user: User): Promise<void> {
    let dbUser = new DbUser(user)
    await dbUser.save()
    return
}

export { getUser, addUser,hasAdmin, }