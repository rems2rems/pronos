# Boilerplate pour projet NoSQL

## Pour lancer le projet:

Installer les dépendances:
```bash
cd pronos
npm i -D
```
Lancer VsCode:
```bash
code .
```
Lancer la compilation en continu de Typescript vers JS:
```bash
npm run dev
```
Pour nettoyer le code généré:
```bash
npm run clean
```
Créer et/ou mettre à jour un fichier .env à la racine (renommer .env.template en .env, et changer les valeurs).

Lancer le serveur (terminal séparé):
```bash
npm start
```

## pour lancer une base Mongo avec Docker:

### avec authentification:

```bash
docker run -d --name mongodb \
      -e MONGO_INITDB_ROOT_USERNAME=admin \
      -e MONGO_INITDB_ROOT_PASSWORD=azerty \
      mongo
```

### sans authentification:

```bash
docker run -d --name mongodb_nologin mongo
```

## Pour lancer /arrêter mongo:

```bash
docker start mongodb
docker stop mongodb
```
ou bien:

```bash
docker start mongodb_nologin
docker stop mongodb_nologin
```
## Pour connaître l'IP d'un container:
```bash
docker container inspect -f '{{ .NetworkSettings.IPAddress }}' mongodb_nologin
```